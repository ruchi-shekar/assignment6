import os
import time

import flask
from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


# Create the application.
application = flask.Flask(__name__)
APP = application

def get_db_creds():
    db = os.environ.get("DB") or os.environ.get("database")
    username = os.environ.get("USER") or os.environ.get("username")
    password = os.environ.get("PASSWORD") or os.environ.get("password")
    hostname = os.environ.get("HOST") or os.environ.get("dbhost")
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title varchar(255), director varchar(255), actor varchar(255), release_date varchar(255), rating DOUBLE, PRIMARY KEY (id), UNIQUE(title))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


################################
# Methods to implement actions #
################################

@APP.route('/')
def home():
    return flask.render_template('index.html')

@APP.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received insert request.")
    yr = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    rel_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES ('" + yr + "', '" + title + "', '" + director + "', '" + actor + "', '" + rel_date + "', '" + rating + "')")

        cnx.commit()
        query_result = ('Movie %s successfully inserted' % (title))
    except Exception as e:
        query_result = ('Movie %s could not be inserted because %s' % (title, e))
    return flask.render_template('index.html', message=query_result)

@APP.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received insert request.")
    yr = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    rel_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT * FROM movies WHERE lower(title)='" + title.lower() + "'");
        if len(cur.fetchall()) != 0:
            cur.execute("UPDATE movies SET director='" + director + "', actor='" + actor + "', release_date='" + rel_date + "', rating='" + rating + "' WHERE year='" + yr + "' AND lower(title)='" + title.lower() + "'")
            cnx.commit()
            query_result = ('Movie %s successfully updated' % (title))
        else:
            query_result = ('Movie %s does not exist' % (title))
    except Exception as e:
        query_result = ('Movie %s could not be updated because %s' % (title, e))

    return flask.render_template('index.html', message=query_result)

@APP.route('/search_movie', methods=['POST'])
def search_movie():
    print("Received search request.")
    actor = request.form['search_actor']


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT title, year, actor FROM movies WHERE lower(actor)='" + actor.lower() + "'");
        result = cur.fetchall()
        if len(result) == 0:
            query_result = ('No movies found for actor %s' % (actor))
        else:
            query_result = ""
            for row in result:
                query_result += ("<b>Title:</b> %s  ") % row[0] 
                query_result += ("<b>Year:</b>  %s  ") % row[1]
                query_result += ("<b>Actor:</b> %s  ") % row[2]
                query_result += '\n'
            query_result = query_result.replace('\n','<br>')
    except Exception as e:
        query_result = ('Could not search actor %s because %s' % (actor, e))

    return flask.render_template('index.html', message=query_result)

@APP.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received delete request.")
    title = request.form['delete_title']


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()


    try:
        cur.execute("SELECT * FROM movies WHERE lower(title)='" + title.lower() + "'");
        if len(cur.fetchall()) != 0:
            cur.execute("DELETE FROM movies WHERE lower(title)='" + title.lower() + "'");
            cnx.commit()
            query_result = ('Movie %s successfully deleted' % (title))
        else:
            query_result = ('Movie %s does not exist' % (title))
    except Exception as e:
        query_result = ('Movie %s could not be deleted because %s' % (title, e))

    return flask.render_template('index.html', message=query_result)


@APP.route('/highest_rating', methods=['GET'])
def print_highest_movie():
    print("Received highest rating request.")


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT title, year, director, actor, rating FROM movies WHERE rating=(SELECT MAX(rating) FROM movies)");
        result = cur.fetchall()
        query_result = ""
        for row in result:
            query_result += ("<b>Title:</b> %s  ") % row[0] 
            query_result += ("<b>Year:</b>  %s  ") % row[1]
            query_result += ("<b>Director:</b>  %s  ") % row[2]
            query_result += ("<b>Actor:</b> %s  ") % row[3]
            query_result += ("<b>Rating:</b>  %s  ") % row[4]
            query_result += '\n'
        query_result = query_result.replace('\n','<br>')
    except Exception as e:
        query_result = ('Failed because %s' % (e))

    
    return flask.render_template('index.html', message=query_result)

@APP.route('/lowest_rating', methods=['GET'])
def print_lowest_movie():
    print("Received lowest rating request.")


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT title, year, director, actor, rating FROM movies WHERE rating=(SELECT MIN(rating) FROM movies)");
        result = cur.fetchall()
        query_result = ""
        for row in result:
            query_result += ("<b>Title:</b> %s  ") % row[0] 
            query_result += ("<b>Year:</b>  %s  ") % row[1]
            query_result += ("<b>Director:</b>  %s  ") % row[2]
            query_result += ("<b>Actor:</b> %s  ") % row[3]
            query_result += ("<b>Rating:</b>  %s  ") % row[4]
            query_result += '\n'
        query_result = query_result.replace('\n','<br>')
    except Exception as e:
        query_result = ('Failed because %s' % (e))
    
    return flask.render_template('index.html', message=query_result)

if __name__ == '__main__':
    APP.debug=True
    APP.run(host='0.0.0.0')